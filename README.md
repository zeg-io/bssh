#BSSH (🐝SSH)

Bulk SSH is a CLI for executing remote commands via SSH on multiple servers.

`Usage: bssh [options] "<command to execute remotely>"`

|Options|Description|
|:---|:---|
|  -V, --version |                  output the version number|
|  -f, --file [filepath] |          What file contains the SSH commands to run the command with?|
|  -i, --identity [identity_file]|  The path to your keypair file|
|  -s, --synchronous              | Just run everything and let results come back in live.  More difficult to read results, but more efficient to run commands.|
|  -u, --user [username]          | The username you wish to use|
|  -h, --help                     | output usage information|

### Example
In certain circumstances you just want to go directly to a specific docker instance (say to get to a shell within it)

Rather than:

1. `ssh -i somekey.key ubuntu@swarmmaster.domain.com`
2. `sudo docker service ls`
3. `sudo docker service ps k28s53gd52fd`
4. `exit`
5. `ssh -i another.key ubutu@worker3.domain.com`
6. `sudo docker exec -it 781nd7s5g2s ash`

Use BSSH

1. **`bssh -f ./production-server-list -i somecommon.key -u ubuntu "sudo docker ps"`**
2. `ssh -i somecommon.key ubuntu@worker3.domain.com`
3. `sudo docker exec -it 781nd7s5g2s ash`

If you have an SSH config set up it is even simpler as you can cut out the key and user:

1. **`bssh -f ./production-server-list "sudo docker ps"`**
2. `ssh -i somecommon.key ubuntu@worker3.domain.com`
3. `sudo docker exec -it 781nd7s5g2s ash`