const { spawn } = require('child_process'),
      chalk = require('chalk')
const log = require('./log')

const run = (command, serverName) => {
  let splitCommand = command.split(' '),
      rootCommand = splitCommand[0]

  splitCommand.shift()

  const runCommand = spawn(rootCommand, splitCommand)

  return spawnEvents(runCommand, serverName)
}

const spawnEvents = (spawnObject, serverName) => new Promise((resolve, reject) =>{
  spawnObject.stdout.on('data', data => {
    log(`${chalk.cyanBright(serverName)}:  ${data}`, 'reset', false)
  })
  spawnObject.stderr.on('data', data => {
    log(`${chalk.cyanBright(serverName)}:  ${chalk.red(data)}`, 'reset', false)
  })
  spawnObject.on('exit', code => {
    if (code !== 0)
      reject(code)
    else
      resolve()
  })
})

module.exports = run
