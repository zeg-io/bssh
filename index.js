const argv = require('commander'),
      chalk = require('chalk'),
      fs = require('fs')
const { exec, spawn } = require('child_process')
const run = require('./spawnEvents'),
      log = require('./log')

argv
.version('1.0.0')
.usage('[options] "<command to execute remotely>"')
.option('-f, --file [filepath]', 'What file contains the SSH commands to run the command with?')
.option('-i, --identity [identity_file]', 'The path to your keypair file')
.option('-s, --synchronous', 'Just run everything and let results come back in live.  More difficult to read results, but more efficient to run commands.')
.option('-u, --user [username]', 'The username you wish to use')
.parse(process.argv)

if (!argv.file) {
  log("Yeah, I'm gonna need you to use the -f flag and provide a file with a list of servers you want to run commands on, one per line, yeah...", 'red')
  process.exit(1)
}

const command = argv.args
let servers = fs.readFileSync(argv.file, 'utf8')
  .trim()
  .split('\n')

log(chalk.yellow(`Command being executed: ${chalk.cyan(command)}`))
log(chalk.yellow('Target Server(s):'))
log(chalk.cyan(servers.join('\n')))

const timeDiff = startTime => Math.round((new Date() - startTime) / 1000)

const syncServerCalls = async (serverArray, commandToRun) => {
  for (const server of serverArray) {
    let fullCommand,
        identity = '',
        user = ''

    if (argv.identity)
      identity = `-i ${argv.identity} `
    if (argv.user)
      user = `${argv.user}@`

    fullCommand = `ssh ${identity}${user}${server} ${commandToRun}`

    log(`\nExecuting command [${chalk.cyan(fullCommand)}]...`)

    const startTime = new Date()

    try {
      await run(fullCommand, server)

      log(`${chalk.cyanBright(server)}:  <process completed: ${timeDiff(startTime)} seconds>`)
    } catch (code) {
      log(`${chalk.cyanBright(server)}:  ${chalk.red(`Child process exited with code ${code.toString()}`)}`)
      log(`${chalk.cyanBright(server)}:  ${chalk.red(`<process failed: ${timeDiff(startTime)} seconds>`)}`)
    }
  }
}

const asyncServerCalls = (serverArray, commandToRun) => {
  for (const server of serverArray) {
    let fullCommand,
      identity = '',
      user = ''

    if (argv.identity)
      identity = `-i ${argv.identity} `
    if (argv.user)
      user = `${argv.user}@`

    fullCommand = `ssh ${identity}${user}${server} ${commandToRun}`

    log(`\nExecuting command [${chalk.cyan(fullCommand)}]...`)

    const startTime = new Date()

    run(fullCommand, server)
    .then(() => {
      log(`${chalk.cyanBright(server)}:  <process completed: ${timeDiff(startTime)} seconds>`)
    })
    .catch(code  => {
      log(`${chalk.cyanBright(server)}:  ${chalk.red(`Child process exited with code ${code.toString()}`)}`)
      log(`${chalk.cyanBright(server)}:  ${chalk.red(`<process failed: ${timeDiff(startTime)} seconds>`)}`)
    })
  }
}

if (!argv.synchronous)
  syncServerCalls(servers, command)
else
  asyncServerCalls(servers, command)
