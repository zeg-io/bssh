const chalk = require('chalk')
const log = (message, color = 'reset', appendNewLine = true) => process.stdout.write(`${chalk[color](message)}${appendNewLine ? '\n' : ''}`)

module.exports = log
